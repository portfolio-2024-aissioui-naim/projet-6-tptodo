# CodeIgniter 4

-Model : les classes métiers en PHP  
-View : les WinForms, les pages html  
-Controller: C# les évenements de la WinForm, c'est aussi une classe en php  
-Dao : les classes d'accès à la base de donnée  

----
SITE To-Do :
```plantuml
@startuml
left to right direction


actor "utilisateur" as user

rectangle To-Do {
  usecase "Voir les tâches" as C1
  usecase "Ajouter tâche" as C2
  usecase "Modifier tâche" as C3
  usecase "Supprimer tâche" as C4
  usecase "Changer l'ordre des tâches" as C5
  usecase "Valider la tâche" as C6
}

user --> C1 
user --> C2
user --> C3 
user --> C4
user --> C5
user --> C6

@enduml

```
Nous sommes redirigés sur la page index :  
![](images/index.png)

<br>

Nous pouvons ajouter une nouvelle tâche :  
![](images/Cree.png)

![](images/AfterCree.png)

<br>

Enfin on peut réordonner l'ordre de la liste des tâches :  
![](images/Reordonne.png)

