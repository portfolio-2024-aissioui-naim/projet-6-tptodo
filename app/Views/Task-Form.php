<?= $this->extend('Page.php')?>
<?= $this->section('body')?>
<div class="card">
    <div class="card-header">
        <?= (isset($task) ? "Modifier une tâche" : "Nouvelle tâche") ?>
    </div>
    <div class="card-body">
        <?= ((session()->has('errors')) ? \Config\Services::validation()->listErrors():'')?>
        <form class ="form-horizontal" action="<?= (isset($task) ? '/sauvegarder/'.$task->id : '/sauvegarder') ?> "method="post">
            <div class=form-group>
                <form-label for="text">Text : </form-label>
                <input type="text" name="text" id="text" value="<?= old('text', $task->text ?? '',false)?> "placeholder="Au boulot">
                <br>
                <form-label for="text">Ordre : </form-label>
                <input type="number" name="order" id="order" value="<?= old('order', $task->order ?? '',false)?> "placeholder="Un numéro">
            </div>
            <button class="btn btn-primary" type="submit">
                <i class="fa fa-plus">Valider</i>
            </button>
        </form>
    </div>
</div>
<?=$this->endSection()?>