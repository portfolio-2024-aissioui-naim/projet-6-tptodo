<?php

namespace App\Controllers;
use App\Entities\Task;
use App\Controllers\BaseController;
use App\Models\TaskModel;

class TaskController extends BaseController
{
    public function __construct(){
        $this->helpers = ['form','url'];
        $this->taskModel = new TaskModel();
    }

    public function index() {   
        $tasks = $this->taskModel ->orderBy('order')->findAll();
        // TP 1 $data['tasks']=$this->create_jeu_essai();
        $data['tasks']=$tasks;
        $data['titre']="au boulot";
        return view('Task-index.php',$data);
    }

    public function create(){
        $data['titre'] = 'Nouvelle Tâche';
        $info = [
            'ip_address' => $this->request->getIPAddress(),
        ];
        
        log_message('info', 'creation d\'une tache ip : {ip_address}', $info);
        return view('Task-Form.php',$data);
    }

    public function delete(int $id){
        $this->taskModel->where(['id'=> $id])->delete();
        $info = [
            'ip_address' => $this->request->getIPAddress(),
        ];
        
        log_message('info', 'suppression d\'une tache ip : {ip_address}', $info);
        return redirect()->to('/')->with('message', 'Tâche supprimée');
    }
    
    public function save(int $id = null){
        $rules = $this->taskModel->getValidationRules();
        if (!$this->validate($rules)){
            return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
        }
        else{
            $form_data = [
                'text' => $this->request->getPost('text'),
                'order' => $this->request->getPost('order'),
            ];
            if (!is_null($id)){
                $form_data['id']=$id;
            }
            $task = new Task($form_data);
            $this ->taskModel->save($task);
            $info = [
                'ip_address' => $this->request->getIPAddress(),
            ];
            
            log_message('info', 'sauvegarde d\'une tache ip : {ip_address}', $info);
            return redirect()->to('/')->with('message', "Tâche sauvegardée");
        }
    }

    public function edit(int $id){
        $data['titre'] = "Modifier tâche";
        // on récupère la tâche à modifier
        $data['task']= $this->taskModel->find($id);
        $info = [
            'ip_address' => $this->request->getIPAddress(),
        ];
        
        log_message('info', 'modification d\'une tache ip : {ip_address}', $info);
        // on appele la vue
        return view('Task-Form.php',$data);
    }

    public function done(int $id){
        $this->taskModel->update($id,['done'=>'1']);
        return redirect()->to('/')->with('message','Tâche faite');
    }

    public function indexReorder(){
        $tasks = $this->taskModel->orderBy('order')->findAll();
        $index = 10;
        // on renumérote l'ordre de toutes les tâches.
        foreach ($tasks as $task){
            $task->order=$index;
            $index+=10;
        }
        $data['tasks'] = $tasks;
        $data['titre'] = "Réordonner les tâches";
        return view('Task-IndexReorder.php', $data);
    }

    public function saveReorder(){
        $validation = \Config\Services::validation();
        $validation->setRule('order.*','ordre','required|numeric');
        if (!$validation->withRequest($this->request)->run()) {
            return redirect()->back()->withInput()->with('errors',$validation->getErrors());
        }
        else{
            $orders = $this->request->getPost('order[]');
            $ids = $this->request->getPost('id[]');
            $index = 0;
            foreach($ids as $id){
                $form_data = [
                    'order' => $orders[$index],
                    'id'    => $ids[$index],
                ];
                $task = new Task($form_data);
                $this->taskModel->save($task);
                $index++;
            }
            return redirect()->to('/')->with('message', "Tâches réorganisées");
        }
    }

    // TP 1
    private function create_jeu_essai(){
        return [
            (object)['text'=>"pipi",'id'=>1],
            (object)['text'=>"les dents",'id'=>2],
            (object)['text'=>"au dodo",'id'=>3],
        ];
    }
}
